package router

import (
	"github.com/gin-gonic/gin"
)

// 路由接口
type Router interface {
	Route(r *gin.Engine)
}

type RegisterRouter struct {
}

func New() *RegisterRouter {
	return &RegisterRouter{}
}

func (*RegisterRouter) Route(ro Router, r *gin.Engine) {
	ro.Route(r)
}

var routers []Router

/**
 * 初始化路由
 */
func InitRouter(r *gin.Engine) {
	//rr := New()
	//rr.Route(&member.RouterMember{}, r)
	for _, rr := range routers {
		rr.Route(r)
	}
}

func Register(rr ...Router) {
	routers = append(routers, rr...)
}
