package main

import (
	_ "all/member/api"
	"all/member/common"
	"all/member/router"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	router.InitRouter(r)
	common.Run(r, "member", ":80")
}
