package dao

import (
	"context"
	"github.com/go-redis/redis/v8"
	"time"
)

var RC *RedisCache

type RedisCache struct {
	rdb *redis.Client
}

func init() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	RC = &RedisCache{
		rdb: rdb,
	}
}

func (*RedisCache) Put(ctx context.Context, key, value string, expire time.Duration) error {
	err := RC.rdb.Set(ctx, key, value, expire).Err()
	return err
}

func (*RedisCache) Get(ctx context.Context, key string) (string, error) {
	result, err := RC.rdb.Get(ctx, key).Result()
	return result, err
}
