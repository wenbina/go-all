package member

import (
	"all/member/router"
	"github.com/gin-gonic/gin"
	"log"
)

func init() {
	log.Println("init member router")
	router.Register(&RouterMember{})
}

type RouterMember struct {
}

func (*RouterMember) Route(r *gin.Engine) {
	handler := New()
	// 获取验证码
	r.POST("/member/get/captcha", handler.getCaptcha)

}
