package member

import (
	"all/member/common"
	"all/member/pkg/dao"
	"all/member/pkg/model"
	"all/member/pkg/repo"
	"context"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

type HandlerMember struct {
	cache repo.Cache
}

func New() *HandlerMember {
	return &HandlerMember{
		cache: dao.RC,
	}
}

// 获取验证码
func (handler *HandlerMember) getCaptcha(ctx *gin.Context) {
	response := &common.Result{}
	// 1：获取参数
	mobile := ctx.PostForm("mobile")
	// 2：校验参数
	if !common.VerifyMobile(mobile) {
		ctx.JSON(http.StatusOK, response.Fail(model.NoLegalMobileCode, model.NoLegalMobileRemark))
	}
	// 3：生成短信验证码
	code := 123456
	// 4：调用短信平台（放入go协程中执行，接口快速响应）
	go func() {
		time.Sleep(2 * time.Second)
		log.Println("短信平台调用成功，发送短信")
		// 5：存储验证码到redis中，过期时间为5分钟
		context, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()
		err := handler.cache.Put(context, "REGISTER_"+mobile, string(rune(code)), 5*time.Minute)
		if err != nil {
			log.Printf("redis存储验证码出现异常：%v", err)
		}
		log.Printf("将手机号和验证码存入redis成功：手机号：%s: 验证码：%v", mobile, code)
	}()
	ctx.JSON(http.StatusOK, response.Success(code))
}
